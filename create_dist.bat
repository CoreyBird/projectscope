@echo off
set /p id="Did you update the setup.py version?"
set /p id="Did you update the setup.py download_url?"
set /p id="Did you remove the old version?"
python setup.py sdist bdist_wheel
python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
RD /S /Q "ProjectScope.egg-info"
echo "Now upload to bitbucket.org
pause