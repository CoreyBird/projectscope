import os
import sys
import subprocess

# Pre-check that the repository is clean - we don't want to accidentally add and release
# temporary code, or perhaps the developer forgot to commit some work.
if("working directory clean" not in os.popen('git status').read()):
    print("Working directory is not clean! Please clean up the directory before making a release!")
    sys.exit(0)

# Determine what release version should be made. Assume we're making a micro release. Get the
# version from a git command, and then increment the minor revision by default.
last_git_tag = os.popen('git describe').read()
last_version = last_git_tag.split('/')[1].split('-')[0]
branch_type = last_git_tag.split('/')[0]
v_major = int(last_version.split('.')[0])
v_minor = int(last_version.split('.')[1])
v_micro = int(last_version.split('.')[2])
v_micro += 1

new_tag = "{branch_type}/{major}.{minor}.{micro}".format(
    branch_type=branch_type,
    major=v_major,
    minor=v_minor,
    micro=v_micro)

# Prompt user for version override
print("Auto-generated tag: " + str(new_tag))
if("n" != input("Override this tag? (y/n): ")):
    continue_editing_tag_string = True
    while continue_editing_tag_string:
        new_tag = input("Please enter a new tag string: ")
        print("You entered: " + new_tag)
        if("y" == input("Accept this tag and continue? (y/n): ")):
            continue_editing_tag_string = False

version_string = new_tag.split('/')[1]
print("New tagged version being created (v{})".format(version_string))

# Update setup.py
os.rename("setup.py", "setup.py.bak")
with open("setup.py.bak", "r") as old_settings_file:
    with open("setup.py", "w") as new_settings_file:
        for line in old_settings_file.readlines():
            # Check for lines that contain the version string. These are the the 'version' line
            # and the 'download_url' line.
            if('version="' in line):
                line = '    version="{}",\n'.format(version_string)
            if('download_url="' in line):
                line = '    download_url="https://bitbucket.org/CoreyBird/projectscope/src/master/dist/ProjectScope-{}.tar.gz",\n'.format(
                    version_string)
            new_settings_file.write(line)
os.remove("setup.py.bak")

# Update the version_info.txt file
os.rename(os.path.join("ProjectScope", "version_info.txt"), os.path.join("ProjectScope", "version_info.txt.bak"))
with open(os.path.join("ProjectScope", "version_info.txt.bak"), "r") as old_version_file:
    with open(os.path.join("ProjectScope", "version_info.txt"), "w") as new_version_file:
        line_number = 0
        for line in old_version_file.readlines():
            line_number += 1
            # Replace the first line of the file with the version string
            if(1 == line_number):
                new_version_file.write(version_string)
os.remove(os.path.join("ProjectScope", "version_info.txt.bak"))

# Delete old distributions
for file in [os.path.abspath(os.path.join("dist", file)) for file in os.listdir("dist") if(os.path.isfile(os.path.abspath(os.path.join("dist", file))))]:
    os.remove(file)

# Create new distribution package
os.system("python setup.py sdist bdist_wheel")

# Clean up temporary files
os.system('RD / S / Q "ProjectScope.egg-info"')
os.system('RD / S / Q "build"')

# Add all files expected to be changed
os.system('git add -A')

# Make a release commit
os.system('git commit -m "Auto-Generated Release"')

# Create tag on the release commit
os.system('git tag -a {} -m "Auto-Generated Release"'.format(new_tag))

# Push commit to online Repo
current_branch = os.popen('git rev-parse --abbrev-ref HEAD').read()
os.system('git push origin {}'.format(current_branch))

# Push tag to online Repo
os.system('git push origin {}'.format(new_tag))

# Push update to PyPi
os.system('python -m twine upload --repository ProjectScope dist/*')
