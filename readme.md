
Project Scope
============================================

Welcome to the Project Scope management tool
--------------------------------------------

--------------------------------------------

This tool is created to help manage the semantic connections in a project. The idea
is that there are many kinds of data required by a project, and sometimes the organization
of ideas cannot be effectively conveyed with a hierarchical file system. Therefore this
tool was written to allow the flexibility in how information is linked together.

This project is written in python, and is biased towards python workflows. There is an
import system built around the python importer utilities that allows symantic linking
between python files.

This project is a real work in progress. This was hastily built to solve a problem I was
having, and as a result many aspects of the project are sub-par.


Usage Guide
-----------

-----------

ToDo


Supported Commands:
-------------------

-------------------

```
~> python -m ProjectScope Initialize
```
Creates a new project in the current working directory.


```
~> python -m ProjectScope Viewer
```
Open a 3D project visualizer application. Requires a GUI and a .projectscope database in the current working directory.

```
~> python -m ProjectScope Upgrade
```
Interrogates the database in the current working directory and attemps an upgrade process if the version in the database
is less than the current version of ProjectScope.


Supported Code Accessors:
-------------------

-------------------

```
import ProjectScope
```
the ```import ProjectScope``` statement will look for a .projectscope database in the current working directory,
and if it doesn't find it then it will start recursing up the folder structure looking for a database. Once the
database is found, you're free to utilize the ProjectScope features.

```
ProjectScope.load("import link1/link2/app2")
```
You can import python modules using ProjectScope using the ```ProjectScope.load``` function. This will add modules
to your python namespace just like a normal import would.

```
ProjectScope.get_node_path("import link1/link2/app2")
```
You can get the node path from code using the ```ProjectScope.get_node_path``` function. This could be useful for
situations where the path contains some kind of configuration information (such as a config file, or some kind of
ip:port information).


Roadmap:
-------------------

-------------------

This roadmap is a living document and outlines the rough priority that new features will be developed in:
* Add 'entangled' nodes which share a single node, but can be visually separated
* Add support for web links
* Add tags to Nodes & Links in the Viewer
* Add ability to filter Nodes & Links (by various properties, including tags) in the Viewer
* Add container nodes that can encapsulate parts of a project, allow to filter by container
* Fix potential concurrency issues if python code and the Viewer utility are both accessing the database at the same time
* Add more shapes/objects to the visualizer
* Add helper tools to layout new nodes faster (ie auto-snap to grid, alignment to existing node tools, duplicate, etc)
* Add multiple views so that nodes can be layed out differently for different perspectives on the project